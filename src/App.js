import React, {
  Component
} from 'react';
import './App.css';
import './ipldata.js';

class App extends React.Component {
  constructor(props) {
    
    super(props);
    this.state={display:false};
    this.handleClick =this.handleClick.bind(this);
    
  }
  handleClick(e){
    this.setState((prevState) => ({
      display:!prevState.display
       }));
  }
  

    render(){
      return (
     <div><h2 onClick={this.handleClick}>SEASON</h2>
     { this.state.display && <Season season ={this.props.data}/>}
     </div>
      );
     
    }
 }
 

class Team extends React.Component{
  constructor(props){
    super(props);
    this.state = { id:"",
  display:true};

   this.handleClick=this.handleClick.bind(this);
   
  };

  handleClick(e){
    var display;
  var id =e.target.id;
  if(id==this.state.id){
     display =false;
  }else{
    display=true;
  }
  this.setState((prevState) => ({
 id:id,
 display:display
  }));
  }

  
  render(){
    
    var team=[];
    for (var list in this.props.data){
      var each_team_in_season =this.props.data[list];
      var team_record =each_team_in_season.map(function(data, index){
        return <li key={index .toString()}>{data}</li>

      });
//   var   team_ul=<ul>{   this.state.display&& this.state.id && team_record}</ul>

      const l =<ul key={list.toString()}><li  id ={list.toString()} onClick={this.handleClick}>{list}</li>
     { (this.state.id==list) && this.state.display && <ul key={list.toString()}>{team_record}</ul>}</ul>
      team.push(l);
    }
    
  return (
   <ul>{team}</ul>
  );
}
}

class Season extends React.Component{
  constructor(props)
  {
    super(props);
    this.state = {display: true, id:""};
    this.handleClick = this.handleClick.bind(this);
  };
 
 handleClick(e){
   var id =e.target.id;
   if(e.target.id ==this.state.id){
    var display =!this.state.display;
   }else{
     
    display =true;
   }
   this.setState((prevState) => ({
    display:display,
   id:id
  }));
  }

  render(){
  
    var total_season=this.props.season;
  
 var season =[];
 
 for(var list in total_season){
     var season_button = <button  id={list.toString()}
      type="button" className=" glyphicon glyphicon-plus-sign" onClick={this.handleClick}>
  </button>
  
  var title= <span className ="season" >{list}</span>
 console.log(list);
   var list_of_season = <ul key={list.toString()}><li >  {season_button} {title}</li>
   
   { (  (this.state.display)&&(this.state.id==list)) &&
    <li key={  {list}+total_season[list].toString()}>{<Team data={total_season[list]}  />}</li>
 }</ul> 
 
 season.push(list_of_season);
  

 }
    
    return (
    
  
<ul >{season}</ul>
    
    );
  }


}



  
  


 



  export default App;